#include "Fibonacci.hpp"


int fibonacciRecursif(int step)
{
	if (step == 0)
	{
		return 0;
	}
	else if (step == 1)
	{
		return 1;
	}
	else
	{
		return fibonacciRecursif(step-1)+fibonacciRecursif(step-2);
	}
	

}

int fibonacciIteratif(int step)
{
	int sum = 0, first = 0, second = 1;
	if (step == 0)
	{
		return first;
	}
	else if (step == 1)
	{
		return second;
	}
	else if (step > 1)
	{
		for(int i=1; i<step; i++)
		{
			sum = first + second;
			first = second;
			second = sum;
		}
	}
	return sum;
}