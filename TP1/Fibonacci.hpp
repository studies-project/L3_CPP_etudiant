#ifndef FIBONACCI_HPP_
#define FIBONACCI_HPP_

int fibonacciRecursif(int step);
int fibonacciIteratif(int step);

#endif