#include <iostream>
#include "Fibonacci.hpp"
#include "Vecteur3.hpp"
using namespace std;

int main()
{
	// Vecteur3
	Vecteur3 test {2, 3, 6};
	Vecteur3 test2 {3, 4, 7};
	
	// Fonction afficher(test);
	// Méthode
	test.afficher();
	cout<<"Norme => "<<test.norme()<<endl;
	cout<<"Produit scalaire => "<<produitScalaire(test, test2)<<endl;

	Vecteur3 add = addition(test, test2);
	add.afficher();

	/* Ex Fibonacci
	int x = 50;
	cin>>x;
	cout<<"Recursive => "<<fibonacciRecursif(x)<<endl;
	cout<<"Iterative => "<<fibonacciIteratif(x)<<endl;
	*/

	return 0;
}