#ifndef VECTEUR3_HPP_
#define VECTEUR3_HPP_

#include <iostream>
#include <cmath>

struct Vecteur3 
{
	float firEl, secEl, thiEl;

	void afficher() const;
	double norme() const;
};

// Fonctions à part
void afficher(Vecteur3 test) 
{
	std::cout<<"("<<test.firEl<<", "<<test.secEl<<", "<<test.thiEl<<")"<<std::endl;
}

int produitScalaire(Vecteur3 vec1, Vecteur3 vec2)
{
	return (vec1.firEl*vec2.firEl + vec1.secEl*vec2.secEl + vec1.thiEl*vec2.thiEl);
}

Vecteur3 addition(Vecteur3 vec1, Vecteur3 vec2)
{
	Vecteur3 sum 
	{
		vec1.firEl + vec2.firEl, 
		vec1.secEl + vec2.secEl,
		vec1.thiEl + vec2.thiEl
	};
	return sum;
}

// Méthodes
void Vecteur3::afficher() const
{
	std::cout<<"("<<firEl<<", "<<secEl<<", "<<thiEl<<")"<<std::endl;
}

double Vecteur3::norme() const
{
	return sqrt(pow(firEl, 2) + pow(secEl, 2) + pow(thiEl, 2));
}

#endif