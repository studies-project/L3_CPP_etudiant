#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, testFiboRec)
{
	int result = fibonacciRecursif(7);
	CHECK_EQUAL(13, result);
}

TEST(GroupFibonacci, testFiboIt)
{
	int result = fibonacciIteratif(7);
	CHECK_EQUAL(13, result);
}