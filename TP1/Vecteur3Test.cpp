#include "Vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVecteur3) { };

TEST(GroupVecteur3, testNorme)
{
	Vecteur3 test {2, 3, 6};
	int result = test.norme();
	CHECK_EQUAL(7, result);
}