#include <iostream>
#include "Magasin.hpp"
#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"

using namespace std;

int main()
{
	Magasin tempMag;
	
	/*
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;

	tempMag.ajouterClient("Toto");
	tempMag.ajouterClient("Titi");
	tempMag.ajouterClient("Tata");
	tempMag.ajouterClient("Tutu");
	tempMag.ajouterClient("Tete");
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;
	tempMag.afficherClients();
	cout<<endl;

	tempMag.supprimerClient(2);
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;
	tempMag.afficherClients();
	cout<<endl;

	tempMag.supprimerClient(4);
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;
	tempMag.afficherClients();
	cout<<endl;

	tempMag.supprimerClient(0);
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;
	tempMag.afficherClients();
	cout<<endl;

	tempMag.supprimerClient(1);
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;
	tempMag.afficherClients();
	cout<<endl;

	tempMag.supprimerClient(3);
	cout<<"Nombre de clients = "<<tempMag.nbClients()<<endl;
	tempMag.afficherClients();
	cout<<endl;
	*/

	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;

	tempMag.ajouterProduit("Toto");
	tempMag.ajouterProduit("Titi");
	tempMag.ajouterProduit("Tata");
	tempMag.ajouterProduit("Tutu");
	tempMag.ajouterProduit("Tete");
	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
	tempMag.afficherProduits();
	cout<<endl;

	tempMag.supprimerProduit(2);
	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
	tempMag.afficherProduits();
	cout<<endl;

	tempMag.supprimerProduit(4);
	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
	tempMag.afficherProduits();
	cout<<endl;

	tempMag.supprimerProduit(0);
	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
	tempMag.afficherProduits();
	cout<<endl;

	tempMag.supprimerProduit(1);
	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
	tempMag.afficherProduits();
	cout<<endl;

	tempMag.supprimerProduit(3);
	cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
	tempMag.afficherProduits();
	cout<<endl;
	
	try
	{
		tempMag.supprimerProduit(3);
		cout<<"Nombre de produits = "<<tempMag.nbProduits()<<endl;
		tempMag.afficherProduits();
		cout<<endl;
	}
	catch(string e)
	{
		cout<<"Erreur"<<endl;
	}





	return 0;
}