#include "Magasin.hpp"
#include <iostream>
#include <algorithm>
#include <pair>

Magasin::Magasin() : _idCourantClient(0), _idCourantProduit(0) {}

int Magasin::nbClients() const
{
	return _clients.size();
}

void Magasin::ajouterClient(const std::string & nom)
{
	Client clientBuf(_idCourantClient, nom);
	_clients.push_back(clientBuf);
	_idCourantClient++;
}

void Magasin::afficherClients() const
{
	for (auto i : _clients)
	{
		i.afficherClient();
	}
}

void Magasin::supprimerClient(int idClient)
{
	// Search element
	int i;
	bool foundEl = false;
	for (i=0; i<(int)_clients.size(); i++)
	{
		if (_clients.at(i).getId() == idClient)
		{
			foundEl = true;
			break;
		}
	}
	
	if (foundEl == true)
	{
		// Swap element with the last one
		std::swap(_clients.at(i), _clients.at(_clients.size()-1));

		_clients.pop_back();
	}

	throw std::string("erreur: ce client n'existe pas");
}

int Magasin::nbProduits() const
{
	return _produits.size();
}

void Magasin::ajouterProduit(const std::string & description)
{
	Produit produitBuf(_idCourantProduit, description);
	_produits.push_back(produitBuf);
	_idCourantProduit++;
}

void Magasin::afficherProduits() const
{
	for (auto i : _produits)
	{
		i.afficherProduit();
	}
}

void Magasin::supprimerProduit(int idProduit)
{
	// Search element
	int i;
	bool foundEl = false;
	for (i=0; i<(int)_produits.size(); i++)
	{
		if (_produits.at(i).getId() == idProduit)
		{
			foundEl = true;
			break;
		}
	}
	
	if (foundEl == true)
	{
		// Swap element with the last one
		std::swap(_produits.at(i), _produits.at(_produits.size()-1));

		_produits.pop_back();
		return;
	}

	throw std::string("erreur: ce produit n'existe pas");
}

int Magasin::nbLocations() const
{
	return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit)
{
	for (auto i : _locations)
	{
		if (i._idClient == idClient and i._idProduit == idProduit)
		{
			throw std::string("erreur : location existe déjà");
		}
	}

	Location newLoc(idClient, idProduit);
	_locations.push_back(newLoc);
}

void Magasin::afficherLocations() const
{
	for (auto i : _locations)
	{
		i.afficherLocation();
	}
}

void Magasin::supprimerLocation(int idClient, int idProduit)
{
	// Search element
	int i;
	bool foundEl = false;
	for (i=0; i<(int)_locations.size(); i++)
	{
		if (_locations.at(i)._idClient == idClient and _locations.at(i)._idProduit == idProduit)
		{
			foundEl = true;
			break;
		}
	}
	
	if (foundEl == true)
	{
		// Swap element with the last one
		std::swap(_locations.at(i), _locations.at(_locations.size()-1));

		_locations.pop_back();
		return;
	}

	throw std::string("erreur: cette location n'existe pas");

}

bool Magasin::trouverClientDansLocation(int idClient) const
{
	for (auto i : _locations)
	{
		if (i._idClient == idClient)
		{
			return true;
		}
	}
	return false;
}

std::vector <int> Magasin::calculerClientsLibres() const
{
	// Search for client not in _locations
	
	// copy _clients and take all his index when not found in _loctaions
	std::pair <int, bool> testCli;

	for (auto i : testCli )
	{
		
	}

	for (auto i : _locations)
	{
		testCli.at(i._idClient) = true;
	}

	std::vector <int> freeCli;
	for (int i=0; i<_clients)

}

bool Magasin::trouverProduitDansLocation(int idProduit) const
{

}

std::vector<int> Magasin::calculerProduitsLibres() const
{

}