#include "Location.hpp"
#include <iostream>

Location::Location()
{
	_idClient = 0;
	_idProduit = 0;
}

Location::Location(int idClient, int idProduit)
{
	_idClient = idClient;
	_idProduit = idProduit;
}

void Location::afficherLocation () const
{
	std::cout<<"Location ("<<_idClient<<", "<<_idProduit<<")"<<std::endl;
}