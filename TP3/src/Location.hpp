#ifndef LOCATION_HPP_
#define LOCATION_HPP_

class Location
{
	public:
		int _idClient;
		int _idProduit;
		
		Location();
		Location(int idClient, int idProduit);
		void afficherLocation() const;
};

#endif