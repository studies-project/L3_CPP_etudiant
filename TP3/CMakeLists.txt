cmake_minimum_required( VERSION 3.0 )
project( TP3 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

# programme principal
add_executable( mainTP3.out 
	src/mainTP3.cpp 
	src/Location.cpp 
	src/Client.cpp 
	src/Produit.cpp
	src/Magasin.cpp)
target_link_libraries( mainTP3.out )