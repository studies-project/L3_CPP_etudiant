#include "Inventaire.hpp"

void Inventaire::trier()
{
	// On fait plus d'operator<, maintenant on fait lamba, auto au lieu de std::function<bool>(const Bouteille &, const Bouteille &) dégueulasse
	auto cmp = [] 
	(const Bouteille & b1, const Bouteille & b2) -> 
	bool {
		return b1._nom < b2._nom;
	};
	std::sort(_bouteilles.begin(), _bouteilles.end(), cmp);
}

std::ostream & operator<<(std::ostream & os, const Inventaire & in)
{
	// Référence pour pas recopier
	// for (const Bouteille & b : in._bouteilles) { os << b; }

	// os dans la liste de capture, on donne la référence, f dans un environnement avec os
	auto f = [&os](const Bouteille & b)
	{ os << b; };
	std::for_each(in._bouteilles.begin(), in._bouteilles.end(), f);
	/*
	Version classique avec itérateur :
	for (std::vector<Bouteille>::const_iterator it=i._bouteilles.begin(); it!=i._bouteilles.end(); ++it) { os << *it; }
	*/
	
	return os;
}

std::istream & operator>>(std::istream & is, Inventaire & in)
{
	Bouteille b;
	// Car is retourne référence donc on teste lecture
	while(is >> b)
	{
		in._bouteilles.push_back(b);
	}
	return is;
}