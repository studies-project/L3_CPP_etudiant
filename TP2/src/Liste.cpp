#include "Liste.hpp"

#include <iostream>

Liste::Liste()
{
	noeud = new Noeud();
}

Liste::~Liste()
{
	Noeud * noeudBuf = noeud;
	while (noeudBuf != nullptr)
	{
		Noeud * noeudTemp = noeudBuf;
		noeudBuf = noeudBuf -> suivant;
		delete noeudTemp;
	}
}

void Liste::ajouterDevant(int valeur)
{
	if (noeud == nullptr)
	{
		noeud -> suivant = nullptr;
		noeud -> valeur = valeur;
	}
	else
	{
		Noeud * teteTemp = new Noeud();
		teteTemp -> suivant = noeud;
		teteTemp -> valeur = valeur;
		noeud = teteTemp;
	}
}

int Liste::getTaille() const
{
	int sum = 0;
	Noeud * noeudBuf = noeud;
	while (noeudBuf != nullptr)
	{
		sum++;
		noeudBuf = noeudBuf -> suivant;
	}
	return sum;
}

// We consider indice to begin at 1
int Liste::getElement(int indice) const
{
	int i= 0;
	Noeud * noeudBuf = noeud;
	while (noeudBuf != nullptr && i < indice-1)
	{
		i++;
		noeudBuf = noeudBuf -> suivant;
	}
	return noeudBuf -> valeur;
}

void Liste::afficher() const
{
	Noeud * noeudBuf = noeud;
	while (noeudBuf != nullptr)
	{
		std::cout<<noeudBuf -> valeur<<std::endl;
		noeudBuf = noeudBuf -> suivant;
	}
}