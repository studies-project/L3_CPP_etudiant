#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <string>
#include "Noeud.hpp"

class Liste 
{
	public:
		Noeud * noeud;
	
		Liste();
		~Liste();
		void ajouterDevant(int valeur);
		int getTaille() const;
		int getElement(int indice) const;
		void afficher() const;

};

#endif