#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_

#include <iostream>
#include <gtkmm.h>
#include "Couleur.hpp"

class FigureGeometrique
{
	protected:
		Couleur _couleur;
	
	public:
        FigureGeometrique(const Couleur & couleur);
        // On lie notre classe avec GTK pour les afficher, dépendance et
        // on rend notre méthode virtuelle
        virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const = 0;
		const Couleur & getCouleur() const;
};

#endif
