#include <iostream>
#include <memory>
#include <vector>
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include "Couleur.hpp"
#include "Point.hpp"

using namespace std;

int main()
{
	Point centre;
	centre._x = 100;
	centre._y = 200;

	Point p0;
	p0._x = 50;
	p0._y = 100;

	Point p1;
	p1._x = 25;
	p1._y = 50;

	Point p2;
	p2._x = 150;
	p2._y = 250;
	
	Point p3;
	p3._x = 200;
	p3._y = 300;

	Couleur couleur;
	couleur._r = 0;
	couleur._g = 1;
	couleur._b = 0;

	vector <unique_ptr<FigureGeometrique>> test;

	test.push_back(make_unique <PolygoneRegulier> (couleur, centre, 50, 5));

	test.push_back(make_unique <Ligne> (couleur, p0, p1));

	test.push_back(make_unique <PolygoneRegulier> (couleur, centre, 100, 10));

	test.push_back(make_unique <Ligne> (couleur, p2, p3));

	for (const auto & uPTest : test)
	{
		uPTest -> afficher();
		cout<<endl;
	}

	
	return 0;
}