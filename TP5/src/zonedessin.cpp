#include "zonedessin.h"

ZoneDessin::ZoneDessin()
{
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {630,10}));
    _figures.push_back(new Ligne({1,0,0}, {630,10}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,470}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {10,470}));
    _figures.push_back(new PolygoneRegulier({0,1,0}, {100,200}, 50, 5));
}

ZoneDessin::~ZoneDessin()
{
    _figures.clear();
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context)
{
    for (auto i : _figures)
    {
        i -> afficher(context);
    }
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton *event)
{
    if (event )
    {
        // Il faut qu'on prenne les coordonnées de la souris
        _figures.push_back(new PolygoneRegulier({0,1,0}, {100,200}, 50, 5));
    }
    else
    {
        _figures.pop_back();
    }
    auto window = get_window();
    window -> invalidate(true);
    return true;
}
