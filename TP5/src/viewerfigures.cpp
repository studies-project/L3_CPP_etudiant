#include "viewerfigures.h"

ViewerFigures::ViewerFigures(int argc, char ** argv) : _kit(argc, argv)
{
    _window.set_title("Salut la mifa");
    _window.set_default_size(640, 480);
    _window.add(_dessin);
    _window.show_all();
    // add_events();
}

void ViewerFigures::run()
{
    _kit.run(_window);
}
