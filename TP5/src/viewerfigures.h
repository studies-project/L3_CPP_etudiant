#ifndef VIEWERFIGURES_H
#define VIEWERFIGURES_H

#include <gtkmm.h>
#include "zonedessin.h"

class ViewerFigures
{
    private:
        Gtk::Main _kit;
        Gtk::Window _window;
        ZoneDessin _dessin;

    public:
        ViewerFigures(int argc, char ** argv);
        void run();
};

#endif // VIEWERFIGURES_H
