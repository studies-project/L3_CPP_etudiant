#ifndef ZONEDESSIN_H
#define ZONEDESSIN_H

#include <vector>
#include <gtkmm.h>
#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

class ZoneDessin : public Gtk::DrawingArea
{
    private:
        std::vector <FigureGeometrique*> _figures;
    public:
        ZoneDessin();
        ~ZoneDessin() override;
        bool on_draw(const Cairo::RefPtr<Cairo::Context> & context) override;
        bool gererClic(GdkEventButton* event);
};

#endif // ZONEDESSIN_H
