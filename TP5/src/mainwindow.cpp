#include <gtkmm.h>
#include "viewerfigures.h"

int main(int argc, char ** argv)
{
    ViewerFigures test(argc, argv);
    test.run();
    return 0;
}
