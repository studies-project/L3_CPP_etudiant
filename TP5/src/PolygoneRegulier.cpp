#include "PolygoneRegulier.hpp"

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes) : FigureGeometrique(couleur), _nbPoints(nbCotes)
{
	// Create array and calculate point
	_points = new Point [_nbPoints];
	for (int i=0; i<_nbPoints; i++)
	{
		float alpha = (i * M_PI * 2) / _nbPoints;
		Point temp;
		temp._x = rayon * cos(alpha) + centre._x;
		temp._y = rayon * sin(alpha) + centre._y;
		_points[i] = temp;
	}
}

PolygoneRegulier::~PolygoneRegulier()
{
	delete[] _points;
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context) const
{
    const Couleur & c = getCouleur();
    context -> set_source_rgb(c._r, c._g, c._b);

    const Point & p1 = _points[_nbPoints-1];
    context -> move_to(p1._x, p1._y);

    for (int i=0; i<_nbPoints; ++i)
    {
        const Point & p = _points[i];
        context -> line_to(p._x, p._y);
    }

    context -> stroke();
    /*
	std::cout<<"PolygoneRegulier ";
	std::cout<<_couleur._r<<"_"<<_couleur._g<<"_"<<_couleur._b<<" ";
	for (int i=0; i<_nbPoints; i++)
	{
		std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
	}
	std::cout<<std::endl;
    */
}

int PolygoneRegulier::getNbPoints () const
{
	return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const
{
	return _points[indice];
}
