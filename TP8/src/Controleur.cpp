#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <ostream>
#include <fstream>

Controleur::Controleur(int argc, char ** argv) {
	_vues.push_back(std::make_unique<VueConsole>(*this));

	_vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

	// _inventaire._bouteilles.push_back({"Maredsous", "2013-08-18", 0.75});

	chargerInventaire(" ");

	for (auto & v : _vues)
		v->actualiser();
}

std::string Controleur::getTexte() const
{
	std::ostringstream oss;
	oss << _inventaire;
	return oss.str();
}

void Controleur::run() {
	for (auto & v : _vues)
		v->run();
}

void Controleur::chargerInventaire(const std::string & nomFichier)
{
	/*
	/home/lodocart/Cours/L3/L3_CPP_etudiant/TP8/src/Controleur.cpp:43:43: warning: missing initializer for member ‘Bouteille::_date’ [-Wmissing-field-initializers]
		_inventaire._bouteilles.push_back({line});
											^
	/home/lodocart/Cours/L3/L3_CPP_etudiant/TP8/src/Controleur.cpp:43:43: warning: missing initializer for member ‘Bouteille::_volume’ [-Wmissing-field-initializers]
	*/

	std::ifstream ifs (nomFichier, std::ifstream::in);
	
	std::string line;
	std::getline(ifs, line);
	while (ifs.good())
	{
		_inventaire._bouteilles.push_back({line});
		// std::cout<<"Test"<<std::endl<<line<<std::endl<<std::endl;
		std::getline(ifs, line);
	}

	for (auto & v : _vues)
		v->actualiser();
}


