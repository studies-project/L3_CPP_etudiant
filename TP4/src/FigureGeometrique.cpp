#include "FigureGeometrique.hpp"

FigureGeometrique::FigureGeometrique(const Couleur & couleur) : _couleur(couleur) {}

const Couleur & FigureGeometrique::getCouleur() const
{
	return _couleur;
}

void FigureGeometrique::afficher() const
{
	std::cout<<"FigureGeometrique ";
	std::cout<<_couleur._r<<"_"<<_couleur._g<<"_"<<_couleur._b<<" ";
}