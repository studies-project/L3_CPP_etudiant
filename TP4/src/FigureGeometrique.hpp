#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_

#include <iostream>
#include "Couleur.hpp"

class FigureGeometrique
{
	protected:
		Couleur _couleur;
	
	public:
		FigureGeometrique(const Couleur & couleur);
		virtual void afficher() const;
		const Couleur & getCouleur() const;
};

#endif