#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <iostream>

class Image
{
	private:
		int _largeur;
		int _hauteur;
		int* _pixels;

	public:
		Image(int largeur, int hauteur);
		Image(const Image & img);
		~Image();

		const Image & operator=(const Image & img);

		const int & getLargeur() const;
		const int & getHauteur() const;
		// Accessceur
		int & pixel(int i, int j) const;
		// Modificateur, pour modifier on fait
		/*
		Image img(40, 30);
		img.pixel(20, 10) = 255;
		car la référence c'est comme un pointeur, donc j'ai accès au pointeur pour modifier sa valeur
		*/
		int & pixel(int i, int j);
};

void ecrirePNM(const Image & img, const std::string & nomFichier);

void remplir(Image & img);

Image bordure(const Image & img, int couleur);

#endif