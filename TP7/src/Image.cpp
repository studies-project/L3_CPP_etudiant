#include "Image.hpp"
#include <fstream>
#include <cmath>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur)
{
	_pixels = new int[_largeur*_hauteur];
	for (int i=0; i<_largeur*_hauteur; i++)
	{
		_pixels[i] = 0;
	}
}

Image::Image(const Image & img) : _largeur(img.getLargeur()), _hauteur(img.getHauteur())
{
	_pixels = new int[_largeur*_hauteur];
	for (int i=0; i<_largeur*_hauteur; i++)
	{
		_pixels[i] = img._pixels[i];
	}
}

Image::~Image()
{
	delete[] _pixels;
}

// On doit le définir car sinon si on fait img2 = img 3 = img1 il va pas utiliser au troisième constructeur recopie.
// Pour chaîner les affectations
const Image & Image::operator=(const Image & img)
{
	if (&img != this)
	{
		_hauteur = img._hauteur;
		_largeur = img._largeur;
		// On doit supprimer notre ancien _pixels
		delete[] _pixels;
		_pixels = new int[_largeur * _hauteur];
		for (int i=0; i<_largeur*_hauteur; i++)
		{
			_pixels[i] = img._pixels[i];
		}
	}
	return *this;
}

/* à l'ancienne
int Image::getLargeur() const
{
	return _largeur;
}
*/

const int & Image::getLargeur() const
{
	return _largeur;
}

const int & Image::getHauteur() const
{
	return _hauteur;
}


int & Image::pixel(int i, int j) const
{
	return _pixels[i*_largeur+j];
}

int & Image::pixel(int i, int j)
{
	return _pixels[i*_largeur+j];
}

// faut afficher largeur, hauteur
// valeur de blanc ? afficher toutes les lignes
void ecrirePNM(const Image & img, const std::string & nomFichier)
{
	// Création de l'entête
	std::ofstream ofs(nomFichier);
	ofs << "P2" <<std::endl;
	ofs << img.getLargeur() << " " << img.getHauteur() << std::endl;
	ofs << "255" << std::endl;

	// On peut directement copier nos pixels car correspondent au format PNM mais plus propre avec deux boucles
	for (int i=0; i<img.getHauteur(); i++)
	{
		for (int j=0; j<img.getLargeur(); j++)
		{
			ofs << img.pixel(i, j)<< " ";
		}
		ofs << std::endl;
	}
}

void remplir(Image & img)
{
	double l = img.getLargeur();
	// Boucle contraire
	for (int j=0; j<img.getLargeur(); j++)
	{
		double t = 3*2*M_PI*j / l;
		int c = (cos(t) + 1) * 127;
		for (int i=0; i<img.getHauteur(); i++)
		{
			img.pixel(i, j) = c;
		}
	}
}

Image bordure(const Image & img, int couleur)
{
	Image img2 = img;

	const int lastCol = img2.getLargeur()-1;
	for(int i=0; i<img2.getHauteur(); i++)
	{
		img2.pixel(i, 0) = couleur;
		img2.pixel(i, lastCol) = couleur;
	}

	const int lastLine = img2.getHauteur()-1;
	for (int j=0; j<img2.getLargeur(); j++)
	{
		img2.pixel(0, j) = couleur;
		img2.pixel(lastLine, j) = couleur;
	}
	return img2;
}