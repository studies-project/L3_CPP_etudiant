#include <iostream>
#include "Bibliotheque.hpp"
#include "Livre.hpp"

using namespace std;

int main()
{
	Bibliotheque b;
	Livre t("Petit Pourcet", "Wolah Alford", 1995);
	b.push_back(t);
	b.afficher();
	return 0;
}

