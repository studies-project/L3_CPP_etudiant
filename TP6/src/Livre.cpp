#include "Livre.hpp"

Livre::Livre() : _titre(""), _auteur(""), _annee(0) {}

Livre::Livre(const std::string & titre, const std::string & auteur, int annee) : _titre(titre), _auteur(auteur), _annee(annee)
{
	// If auteur has a ;
	if ((int)_auteur.find(';') != -1)
	{
		throw std::string("erreur : auteur non valide (';' non autorisé)");
	}

	// If auteur has a \n
	else if ((int)_auteur.find('\n') != -1)
	{
		throw std::string("erreur : auteur non valide ('\n' non autorisé)");
	}

	// If titre has a ;
	else if ((int)_titre.find(';') != -1)
	{
		throw std::string("erreur : titre non valide (';' non autorisé)");
	}

	// If titre has a \n
	else if ((int)_titre.find('\n') != -1)
	{
		throw std::string("erreur : titre non valide ('\n' non autorisé)");
	}
}

const std::string & Livre::getTitre() const { return _titre; }

const std::string & Livre::getAuteur() const { return _auteur; }

int Livre::getAnnee() const { return _annee; }

bool Livre::operator<(const Livre & l2) const
{
	if (_auteur == l2._auteur)
	{
		return _titre < l2._titre;
	}
	return _auteur < l2._auteur;
}

bool operator==(const Livre & l1, const Livre & l2)
{
	return l1.getTitre() == l2.getTitre() and l1.getAuteur() == l2.getAuteur() and l1.getAnnee() == l2.getAnnee();
}

std::ostream & operator<<(std::ostream & os, const Livre & l)
{
	// No std::endl because it's a void function
	os << l.getTitre() << ";" << l.getAuteur() << ";" << l.getAnnee();
	return os;
}
