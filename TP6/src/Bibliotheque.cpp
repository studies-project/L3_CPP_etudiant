#include "Bibliotheque.hpp"

void Bibliotheque::afficher() const
{
	for (Livre i : *this)
	{
		std::cout << i << std::endl;
	}
}

void Bibliotheque::trierParAuteurEtTitre()
{
	std::sort(begin(), end());
}

void Bibliotheque::trierParAnnee()
{
	// We use a lambda function to return the most recent book
	auto cmpLivre = [](const Livre & a, const Livre & b)
	{
		return a.getAnnee() < b.getAnnee();
	};

	// We give lambda function to sort
	std::sort(begin(), end(), cmpLivre);
}

/* A terminer
void Bibliotheque::ecrireFichier(const std::string & nomFichier) const
{
	std::ofstream ofs(nomFichier);
	for (const Livre & l : *this)
	{
		ofs << l << std::endl;
	}
}
*/